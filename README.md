## Create a pronunciation dictionary of subword syllables from running text corpus

Input needed: raw.txt

You need to create a python virtual environment and install `mlphon` python library to run the scripts for phonemization and syllabification
```
python transliterate.py -i raw.txt -o phonemised.txt
```

```
python syllabify.py -i raw.txt -o syllabified.txt
```

```
python dictionarycreator.py -i syllabified.txt -i2 phonemised.txt -o syllables.txt -o2 phonemes.txt
```
### Bash scripts

```
paste syllables.txt phonemes.txt > unsortedLexicon.txt
```
```
sort -u unsortedLexicon.txt -o lexicon.txt
```

To clean up lines with invalid (?) script entries:

```
sed -i "/\?/d" lexicon.txt
sed -i 's/_/\ /g' lexicon.txt
sed "/\?/d" syllabified.txt > text.txt
```

### RESULT

The created lexicon and language model training files are in ./data

> For lexicons and language model training corpus created for interspeech 2022 ASR experiments, see the ./data directory in [syl-subwords](https://gitlab.com/kavyamanohar/malayalam-syl-language-model/-/tree/syl-subwords/data) branch of this repo. The ASR training for these experiments are done using Kaldi and the code is available [here](https://gitlab.com/kavyamanohar/asr-malayalam/-/tree/subword).