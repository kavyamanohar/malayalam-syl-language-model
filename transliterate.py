import mlphon
from mlphon import PhoneticAnalyser, phonemize
import argparse
from sys import stderr, stdin, stdout

def main():
    a = argparse.ArgumentParser()
    a.add_argument('-i', '--input', metavar="INFILE", type=open,
                   dest="infile", help="source of Malayalam running text")
    # കാവ്യ
    # അമ്മ കാവ്യയെ കണ്ടു
    a.add_argument('-o', '--output', metavar="OUTFILE", type=argparse.FileType('w+', encoding='UTF-8'),
                   dest="outfile", help="target of phonemized strings")
    # k_aː_ ʋ_j_a_ 
    # a_ m_m_a_ k_aː_ ʋ_j_a_ j_e_ k_a_ ɳ_ʈ_u_ 
    options = a.parse_args()
    if not options.infile:
        options.infile = stdin
    if not options.outfile:
        options.outfile = stdout
    mlphon = PhoneticAnalyser()
    for line in options.infile:
        line = line.strip()
        if not line or line == '':
            continue
        words = line.split()
        for word in words:
            try:
                analysis = mlphon.analyse(word)
            except ValueError as error_instance:
                options.outfile.write("?"+" ")
            else:
                options.outfile.write(str(phonemize(analysis[0], syllable_end=' ', phoneme_end='_')))
        options.outfile.write("\n")


if __name__ == "__main__":
    main()
