import argparse
from sys import stderr, stdin, stdout 

def main():
    a = argparse.ArgumentParser()
    a.add_argument('-i', '--input', metavar="INFILE", type=open,
                   dest="infile", help="source of syllabified data")
    # കാ@@ വ്യ 
    # അ@@ മ്മ കാ@@ വ്യ@@ യെ ക@@ ണ്ടു 
    a.add_argument('-i2', '--input2', metavar="INFILE2", type=open,
                   dest="infile2", help="source of phonemized data")
    # k_aː_ ʋ_j_a_ 
    # a_ m_m_a_ k_aː_ ʋ_j_a_ j_e_ k_a_ ɳ_ʈ_u_ 
    a.add_argument('-o', '--output', metavar="OUTFILE", type=argparse.FileType('w+', encoding='UTF-8'),
                   dest="outfile", help="target of syllable list")
    # കാ@@
    # വ്യ
    # അ@@
    # മ്മ
    # കാ@@
    # വ്യ@@
    # യെ
    # ക@@
    # ണ്ടു
    a.add_argument('-o2', '--output22', metavar="OUTFILE", type=argparse.FileType('w+', encoding='UTF-8'),
                   dest="outfile2", help="target of phoneme list")
    # k_aː_
    # ʋ_j_a_
    # a_
    # m_m_a_
    # k_aː_
    # ʋ_j_a_
    # j_e_
    # k_a_
    # ɳ_ʈ_u_
    options = a.parse_args()
    for line in options.infile:
        line = line.strip()
        if not line or line == '':
            continue
        words = line.split()
        for word in words:
            options.outfile.write(word+"\n")
    for line in options.infile2:
        line = line.strip()
        if not line or line == '':
            continue
        words = line.split()
        for word in words:
            options.outfile2.write(word+"\n")

if __name__ == "__main__":
    main()