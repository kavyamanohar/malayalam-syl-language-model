import mlphon
from mlphon import PhoneticAnalyser, phonemize
import argparse
from sys import stderr, stdin, stdout

def main():
    a = argparse.ArgumentParser()
    a.add_argument('-i', '--input', metavar="INFILE", type=open,
                   dest="infile", help="source of analysis data")
    # കാവ്യ
    # അമ്മ കാവ്യയെ കണ്ടു
    a.add_argument('-o', '--output', metavar="OUTFILE", type=argparse.FileType('a', encoding='UTF-8'),
                   dest="outfile", help="target of syllabified strings")
    # കാ@@ വ്യ 
    # അ@@ മ്മ കാ@@ വ്യ@@ യെ ക@@ ണ്ടു 
    options = a.parse_args()
    if not options.infile:
        options.infile = stdin
    if not options.outfile:
        options.outfile = stdout
    mlphon = PhoneticAnalyser()
    for line in options.infile:
        line = line.strip()
        if not line or line == '':
            continue
        words = line.split()
        for word in words:
            try:
                syllables = mlphon.split_to_syllables(word)
            except ValueError as error_instance:
                options.outfile.write(word+"? ")
            else:
                options.outfile.write("@@ ".join(syllables)+" ")
        options.outfile.write("\n")

if __name__ == "__main__":
    main()
